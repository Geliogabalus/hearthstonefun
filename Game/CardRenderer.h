#pragma once
#include "SpriteRenderer.h"
#include "TextRenderer.h"
#include <GLFW/glfw3.h>
#include "Card.h"
#include "CardFramebuffer.h"

class CardRenderer
{
public:
	CardRenderer();
	void DrawCard(SpriteRenderer &s_renderer, TextRenderer &t_renderer, GLfloat x, GLfloat y, Card* card);
	~CardRenderer();
};

