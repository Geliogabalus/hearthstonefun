#pragma once
#include "ResourceManager.h"
#include <iostream>
enum CARD_TYPE {
	SPELL,
	MINION
};

struct Stats {
	int cost;
	int health;
	int attack;
};

class Card
{
public:
	int ID;
	Texture2D art;
	std::string Name;
	glm::vec2 artOffset;
	CARD_TYPE type;
	std::string Text;
	Stats baseStats;
	Card(Texture2D& art, glm::vec2 artOffset, CARD_TYPE type, std::string name, std::string text, Stats baseStats, int ID);
	~Card();
};

