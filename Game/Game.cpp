#include "Game.h"


Game::Game(GLuint width, GLuint height)
	: State(GAME_MENU), Keys()
{
	Width = width;
	Height = height;
}

Game::~Game()
{

}

void Game::Init()
{
	// Load shaders
	ResourceManager::LoadShader("shaders/sprite.vs", "shaders/sprite.frag", nullptr, "sprite");
	ResourceManager::LoadShader("shaders/card.vs", "shaders/card.frag", nullptr, "card");
	ResourceManager::LoadShader("shaders/text.vs", "shaders/text.frag", nullptr, "text");
	ResourceManager::LoadShader("shaders/text.vs", "shaders/text_outline.frag", nullptr, "text_outline");
	// Configure shaders
	glm::mat4 projection = glm::ortho(0.0f, static_cast<GLfloat>(this->Width),
		static_cast<GLfloat>(this->Height), 0.0f, -1.0f, 1.0f);

	ResourceManager::GetShader("sprite").Use().SetInteger("image", 0);
	ResourceManager::GetShader("sprite").SetMatrix4("projection", projection);
	ResourceManager::GetShader("sprite").SetInteger("iscard", 0);

	ResourceManager::GetShader("card").Use().SetInteger("image", 0);
	ResourceManager::GetShader("card").SetMatrix4("projection", projection);

	ResourceManager::GetShader("text_outline").Use().SetInteger("text", 0);
	ResourceManager::GetShader("text_outline").SetMatrix4("projection", projection);

	// Set render-specific controls
	SRenderer = new SpriteRenderer(ResourceManager::GetShader("sprite"));
	TRenderer = new TextRenderer(this->Width, this->Height);
	TRenderer->Load("fonts/Belwe-Bold.TTF", 24);
	// Load textures
	ResourceManager::LoadTexture("back.png", GL_FALSE, "back");
	ResourceManager::LoadTexture("board.jpg", GL_FALSE, "board");
	ResourceManager::LoadTexture("btn.png", GL_TRUE, "btn");
	ResourceManager::LoadTexture("cards/n_mask.png", GL_TRUE, "n_mask");
	ResourceManager::LoadTexture("cards/Yeti.jpg", GL_TRUE, "yeti");

	player = new Player();
	opponent = new Player();

	menuUI = new UI(player, opponent);
	menuUI->AddTextPanel("Hearthstone", glm::vec2(0.1, 0.4), glm::vec2(0.2, 0.05), ResourceManager::GetTexture("btn"));
	menuUI->AddButton("Play", glm::vec2(0.1, 0.6), glm::vec2(0.2, 0.05), ResourceManager::GetTexture("btn"), new ChangeGameStateAction(GAME_CARDS, this));

	cardsUI = new UI(player, opponent);
	cardsUI->AddTextPanel("Collection", glm::vec2(0.1, 0.05), glm::vec2(0.2, 0.05), ResourceManager::GetTexture("btn"));
	cardsUI->AddCollectionViewer(glm::vec2(0.1, 0.1), glm::vec2(0.6, 0.8), ResourceManager::GetTexture("btn"));
	cardsUI->AddDeckViewer(glm::vec2(0.7, 0.1), glm::vec2(0.2, 0.8), ResourceManager::GetTexture("btn"));
	cardsUI->AddButton("Back", glm::vec2(0.1, 0.9), glm::vec2(0.2, 0.05), ResourceManager::GetTexture("btn"), new ChangeGameStateAction(GAME_MENU, this));
	cardsUI->AddButton("Fight", glm::vec2(0.3, 0.9), glm::vec2(0.2, 0.05), ResourceManager::GetTexture("btn"), new ChangeGameStateAction(GAME_ACTIVE, this));

	gameUI = new UI(player, opponent);

	CardFramebuffer::InitFramebuffer();
	CardFramebuffer::mainScreen = glm::vec2(this->Width, this->Height);

}

void Game::Update(GLfloat dt)
{
}


void Game::ProcessInput(GLfloat dt)
{
	KeysInput(dt);
	MouseInput(dt);
}

void Game::KeysInput(GLfloat dt) {
	switch (State)
	{
	case GAME_ACTIVE:
		break;
	case GAME_MENU:
		break;
	default:
		break;
	}
}

void Game::MouseInput(GLfloat dt) {
	static bool ispicked;
	switch (State)
	{
	case GAME_CARDS:
		if (this->Mouse[GLFW_MOUSE_BUTTON_LEFT]) {
			cardsUI->CheckButtons(this->MouseCoord, glm::vec2(Width, Height));
		}
		else {
			cardsUI->UnPick();
		}
		cardsUI->UpdateMouse(this->MouseCoord);
		break;
	case GAME_MENU:
		if (this->Mouse[GLFW_MOUSE_BUTTON_LEFT]) {
			menuUI->CheckButtons(this->MouseCoord, glm::vec2(Width,Height));		
		}
		break;
	default:
		break;
	}
}

void Game::Render()
{
	switch (State)
	{
	case GAME_ACTIVE:
		SRenderer->DrawSprite(ResourceManager::GetTexture("board"), glm::vec2(0, 0), glm::vec2(Width, Height));
		gameUI->Draw( *SRenderer, *TRenderer, glm::vec2(Width, Height));
		break;
	case GAME_MENU:
		SRenderer->DrawSprite(ResourceManager::GetTexture("back"), glm::vec2(0, 0), glm::vec2(Width, Height));
		menuUI->Draw( *SRenderer, *TRenderer, glm::vec2(Width, Height));
		break;
	case GAME_CARDS:
		SRenderer->DrawSprite(ResourceManager::GetTexture("back"), glm::vec2(0, 0), glm::vec2(Width, Height));
		cardsUI->Draw( *SRenderer, *TRenderer, glm::vec2(Width, Height));
		break;
	default:
		break;
	}
}