#pragma once
#include "Action.h"
#include "UI.h"
class CloseDeck :
	public Action
{
public:
	void Execute();
	CloseDeck(DeckList* dl);
	~CloseDeck();
private:
	DeckList* dl;
};

