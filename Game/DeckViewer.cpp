#include "DeckViewer.h"
#include "Button.h"
#include "CloseDeck.h"
#include "DeleteDeck.h"

void DeckViewer::AttachDeckList(DeckList * dl)
{
	decklist = dl;
	for (int i = 0; i < dl->Size(); i++) {
		btns.push_back(new Button(decklist->GetDecks()[i]->GetName(), glm::vec2(0, 0), glm::vec2(0, 0), ResourceManager::GetTexture("btn"), new SetCurrentDeck(decklist, i)));
	}
	controlbtns.push_back(new Button("Close", glm::vec2(0, 0), glm::vec2(0, 0), ResourceManager::GetTexture("btn"), new CloseDeck(decklist)));
	controlbtns[0]->Position = glm::vec2(0.7, 0.85);
	controlbtns[0]->Size = glm::vec2(0.1, 0.05);
	controlbtns.push_back(new Button("Delete", glm::vec2(0, 0), glm::vec2(0, 0), ResourceManager::GetTexture("btn"), new DeleteDeck(decklist)));
	controlbtns[1]->Position = glm::vec2(0.8, 0.85);
	controlbtns[1]->Size = glm::vec2(0.1, 0.05);
}

void DeckViewer::Draw(SpriteRenderer & s_renderer, TextRenderer & t_renderer, glm::vec2 screen)
{
	s_renderer.DrawSprite(Texture, glm::round(Position * screen), glm::round(Size * screen));
	if (!decklist->CheckCurrentDeck()) {
		GLfloat x = Position.x + 0.01;
		GLfloat yb = Position.y;
		GLfloat y;
		for (int i = 0; i < btns.size(); i++) {
			y = yb + 0.02 + i * 0.1;
			btns[i]->Position = glm::vec2(x, y);
			btns[i]->Size = glm::vec2(Size.x - 0.02, 0.12);
			btns[i]->Draw(s_renderer, t_renderer, screen);
		}
	}
	else {
		Deck* cdeck = decklist->GetCurrentDeck();
		for (int i = 0; i < controlbtns.size(); i++) {
			controlbtns[i]->Draw(s_renderer, t_renderer, screen);
		}
	}
}

DeckViewer::DeckViewer(glm::vec2 pos, glm::vec2 size, Texture2D texture)
{
	Position = pos;
	Size = size;
	Texture = texture;
}


DeckViewer::~DeckViewer()
{
}

void DeckViewer::BtnCheck(glm::vec2 mouse)
{
	for (int i = 0; i < btns.size(); i++) {
		if (((mouse.x > btns[i]->Position.x) && (mouse.x < btns[i]->Position.x + btns[i]->Size.x)) &&
			((mouse.y > btns[i]->Position.y) && (mouse.y < btns[i]->Position.y + btns[i]->Size.y))) {
			btns[i]->DoCommand();
		}
	}
	if (decklist->CheckCurrentDeck()) {
		for (int i = 0; i < controlbtns.size(); i++) {
			if (((mouse.x > controlbtns[i]->Position.x) && (mouse.x < controlbtns[i]->Position.x + controlbtns[i]->Size.x)) &&
				((mouse.y > controlbtns[i]->Position.y) && (mouse.y < controlbtns[i]->Position.y + controlbtns[i]->Size.y))) {
				controlbtns[i]->DoCommand();
			}
		}
	}
}
