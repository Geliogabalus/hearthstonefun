#include "DeleteDeck.h"



void DeleteDeck::Execute()
{
	int k = dl->GetCurrentDeckInd();
	dl->DeleteDeck(k);
	dl->DropCurrentDeck();
}

DeleteDeck::DeleteDeck(DeckList * dl)
{
	this->dl = dl;
}

DeleteDeck::~DeleteDeck()
{
}
