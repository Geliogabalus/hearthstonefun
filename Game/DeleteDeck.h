#pragma once
#include "Action.h"
#include "UI.h"
class DeleteDeck :
	public Action
{
public:
	void Execute();
	DeleteDeck(DeckList* dl);
	~DeleteDeck();
private:
	DeckList* dl;
};

