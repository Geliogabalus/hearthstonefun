#pragma once
#include "ElementUI.h"
#include "DeckList.h"
#include "SetCurrentDeck.h"
class DeckViewer : 
	public ElementUI
{
public:
	void AttachDeckList(DeckList* dl);
	void Draw(SpriteRenderer &s_renderer, TextRenderer &t_renderer, glm::vec2 screen);
	DeckViewer(glm::vec2 pos, glm::vec2 size, Texture2D texture);
	~DeckViewer();	
	void BtnCheck(glm::vec2 mouse);
private:
	DeckList* decklist;	
	std::vector<ElementUI*> btns;
	std::vector<ElementUI*> controlbtns;
};

