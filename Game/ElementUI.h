#pragma once
#include <glew.h>
#include <glm/glm.hpp>
#include "texture.h"
#include "SpriteRenderer.h"
#include "TextRenderer.h"
#include "ResourceManager.h"
class ElementUI
{
public:
	glm::vec2   Position, Size;
	glm::vec3   Color;
	GLfloat     Rotation;
	Texture2D   Texture;
	ElementUI();
	virtual void Draw(SpriteRenderer &s_renderer,TextRenderer &t_renderer, glm::vec2 screen) = 0;
	virtual void DoCommand();
	~ElementUI();
};

