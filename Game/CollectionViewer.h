#pragma once
#include "ElementUI.h"
#include "Card.h"
#include <vector>
class CollectionViewer :
	public ElementUI
{
public:
	std::vector<std::vector<Card*>> Collection;
	int page;
	int pagescount;
	int currentpage;
	void AddCard(Card*);
	void btnCheck(glm::vec2 mouse);
	void Draw(SpriteRenderer &s_renderer, TextRenderer &t_renderer, glm::vec2 screen);
	CollectionViewer(glm::vec2 pos, glm::vec2 size, Texture2D texture, ElementUI* mainUI);
	~CollectionViewer();
private:
	void CreateCV();
	ElementUI* mainUI;
	std::vector<ElementUI*> cviews;
};

