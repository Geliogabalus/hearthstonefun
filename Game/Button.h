#pragma once
#include "TextPanel.h"
#include "Action.h"

class Button :
	public TextPanel
{
public:
	Button(std::string text, glm::vec2 pos, glm::vec2 size, Texture2D& texture, Action* com) : 
		TextPanel(text, pos, size, texture){
		command = com;
	}
	void DoCommand(); 
	~Button();
private:
	Action* command;
};

