#include "UI.h"
#include "ResourceManager.h"
void UI::AddTextPanel(std::string text, glm::vec2 pos, glm::vec2 size, Texture2D texture)
{
	TPelems.push_back(TextPanel(text, pos, size, texture));
}
void UI::AddCollectionViewer(glm::vec2 pos, glm::vec2 size, Texture2D texture)
{
	CVelems.push_back(CollectionViewer(pos, size, texture, this));
}
void UI::AddDeckViewer(glm::vec2 pos, glm::vec2 size, Texture2D texture)
{
	DVelems.push_back(DeckViewer(pos, size, texture));
	DVelems[DVelems.size() - 1].AttachDeckList(player->GetDeckList());
}
void UI::AddButton(std::string text, glm::vec2 pos, glm::vec2 size, Texture2D texture, Action* com)
{
	Belems.push_back(Button(text, pos, size, texture,com));
}
void UI::Draw(SpriteRenderer &s_renderer, TextRenderer &t_renderer, glm::vec2 screen)
{
	for (int i = 0; i < TPelems.size(); i++) {
		TPelems[i].Draw(s_renderer,t_renderer, screen);
	}
	for (int i = 0; i < Belems.size(); i++) {
		Belems[i].Draw(s_renderer,t_renderer, screen);
	}	
	for (int i = 0; i < CVelems.size(); i++) {
		CVelems[i].Draw(s_renderer,t_renderer, screen);
	}
	for (int i = 0; i < DVelems.size(); i++) {
		DVelems[i].Draw(s_renderer, t_renderer, screen);
	}
	if (pressed && (card != nullptr)) {
		cd_renderer.DrawCard(s_renderer, t_renderer, cardPosition.x, cardPosition.y, card);
	}	
}

void UI::CheckButtons(glm::vec2 mousecoord, glm::vec2 screen)
{
	glm::vec2 mouse = mousecoord / screen;

	for (int i = 0; i < Belems.size(); i++) {
		if (((mouse.x > Belems[i].Position.x) && (mouse.x < Belems[i].Position.x + Belems[i].Size.x)) &&
			((mouse.y > Belems[i].Position.y) && (mouse.y < Belems[i].Position.y + Belems[i].Size.y))) {
			Belems[i].DoCommand();
		}
	}
	
	for (int i = 0; i < DVelems.size(); i++) {
		if (((mouse.x > DVelems[i].Position.x) && (mouse.x < DVelems[i].Position.x + DVelems[i].Size.x)) &&
			((mouse.y > DVelems[i].Position.y) && (mouse.y < DVelems[i].Position.y + DVelems[i].Size.y))) {
			DVelems[i].BtnCheck(mouse);
		}
	}
	for (int i = 0; i < CVelems.size(); i++) {
		if (((mouse.x > CVelems[i].Position.x) && (mouse.x < CVelems[i].Position.x + CVelems[i].Size.x)) &&
			((mouse.y > CVelems[i].Position.y) && (mouse.y < CVelems[i].Position.y + CVelems[i].Size.y))) {
			CVelems[i].btnCheck(mousecoord);
		}
	}
}

void UI::UpdateMouse(glm::vec2 mousecoord)
{
	cardPosition = mousecoord - glm::vec2(100, 135);

}

void UI::Pick(Card* card)
{
	pressed = true;
	this->card = new Card(*card);
}

void UI::UnPick()
{
	pressed = false;
	delete(card);
	card = nullptr;
}

UI::UI(Player * player, Player * opp)
{
	pressed = false;
	cd_renderer = CardRenderer();
	this->player = player;
	this->opponent = opp;
}



UI::~UI()
{
}
