#version 330 core
in vec2 TexCoords;
out vec4 color;

uniform sampler2D image;
uniform vec3 spriteColor;

void main()
{    
	vec4 sC = texture(image, vec2(TexCoords.x, 1 - TexCoords.y));
	if(sC.xyz == vec3(1, 0, 1)){
		sC.a = 0.0;
	}
    color = vec4(sC) * texture(image, vec2(TexCoords.x, 1 - TexCoords.y));
}  