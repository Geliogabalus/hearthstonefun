#version 330 core
in vec2 TexCoords;
out vec4 color;

uniform sampler2D image;
uniform vec3 spriteColor;
uniform int iscard;

void main()
{    
	vec4 sC;
	if(iscard == 1){
		sC = texture(image, vec2(TexCoords.x, 1 - TexCoords.y));
		if(sC.xyz == vec3(1, 0, 1))
		sC.a = 0.0;
		if(sC.a < 0.5){
			sC.a = 0.0;
		}
	}
	else{
		sC = texture(image, vec2(TexCoords.x, TexCoords.y));
	}
    color = vec4(spriteColor, 1.0) * sC;
}  