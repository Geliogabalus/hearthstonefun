#version 330 core
in vec2 TexCoords;
out vec4 color;

uniform sampler2D text;
uniform vec3 textColor;
const float offset = 1.0 / 50; 

void main()
{    
	vec2 offsets[9] = vec2[](
        vec2(-offset, offset),	// top-left
        vec2(0.0f,    offset),  // top-center
        vec2(offset,  offset),  // top-right
        vec2(-offset, 0.0f),    // center-left
        vec2(0.0f,    0.0f),    // center-center
        vec2(offset,  0.0f),    // center-right
        vec2(-offset, -offset), // bottom-left
        vec2(0.0f,    -offset), // bottom-center
        vec2(offset,  -offset)  // bottom-right    
    );

	 float kernel[9] = float[](
        -1, -1, -1,
        -1,  9, -1,
        -1, -1, -1
    );
	
    vec4 sampled = vec4(1.0, 1.0, 1.0, texture(text, TexCoords).r);
    vec4 texcolor = vec4(textColor, 1.0) * sampled;
	for(int i = 0; i < 9; i++)
    {
	    vec4 offsampled = vec4(1.0, 1.0, 1.0, texture(text, TexCoords.st + offsets[i]).r);
		vec4 offtexcolor = vec4(textColor, 1.0) * offsampled;
		if ((texcolor.a > 0.8) && (offtexcolor.a < 0.8)){
			texcolor = vec4(0,0,0,1);
			break;
		}
    }
	color = texcolor;
}  