#pragma once
#include "Button.h"
#include "CardRenderer.h"

class CardView :
	public Button
{
public:
	void Draw(SpriteRenderer &s_renderer, TextRenderer &t_renderer, glm::vec2 screen);
	CardView(std::string text, glm::vec2 pos, glm::vec2 size, Texture2D& texture, Action* com, Card* card) :
		Button(text, pos, size, texture, com) {
		this->card = card;
	};
	~CardView();
private:
	Card* card;
	CardRenderer cd_renderer;
};

