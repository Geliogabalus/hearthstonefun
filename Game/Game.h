#pragma once
#include <glew.h>
#include <GLFW/glfw3.h>
#include "SpriteRenderer.h"
#include "TextRenderer.h"
#include "ResourceManager.h"
#include "Physics.h"
#include "UI.h"
#include "ChangeGameStateAction.h"
#include "Card.h"
#include "CardFramebuffer.h"
#include "Player.h"

// Represents the current state of the game
enum GameState {
	GAME_ACTIVE,
	GAME_CARDS,
	GAME_MENU,
};

// Game holds all game-related state and functionality.
// Combines all game-related data into a single class for
// easy access to each of the components and manageability.
class Game
{
public:
	// Game state
	GameState              State;
	GLboolean              Keys[1024];
	GLboolean              Mouse[12];
	glm::vec2			   MouseCoord;
	GLuint          Width;
	GLuint		   Height;
	// Constructor/Destructor
	Game(GLuint width, GLuint height);
	~Game();
	// Initialize game state (load all shaders/textures/levels)
	void Init();
	// GameLoop
	void ProcessInput(GLfloat dt);
	void KeysInput(GLfloat dt);
	void MouseInput(GLfloat dt);
	void Update(GLfloat dt);
	void Render();
private:
	SpriteRenderer *SRenderer;
	TextRenderer *TRenderer;
	Player* player;
	Player* opponent;
	UI* menuUI;
	UI* cardsUI;
	UI* gameUI;
};