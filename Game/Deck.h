#pragma once
#include <vector>
#include <map>
#include "Card.h"
class Deck
{
public:
	void addCard(Card &card);
	Deck(std::string name);
	std::string GetName() {
		return name;
	}
	~Deck();
private:
	std::string name;
	std::map<int, int> deck;
};

