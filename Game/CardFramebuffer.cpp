#include "CardFramebuffer.h"

GLuint CardFramebuffer::framebuffer;
GLuint CardFramebuffer::texColorBuffer;
glm::vec2 CardFramebuffer::mainScreen;

void CardFramebuffer::InitFramebuffer()
{
	glGenFramebuffers(1, &framebuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);

	glGenTextures(1, &texColorBuffer);
	glBindTexture(GL_TEXTURE_2D, texColorBuffer);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 200, 270, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glBindTexture(GL_TEXTURE_2D, 0);

	// Attach it to currently bound framebuffer object
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texColorBuffer, 0);

	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		std::cout << "ERROR::FRAMEBUFFER:: Framebuffer is not complete!" << std::endl;
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void CardFramebuffer::StartRender()
{
	glm::mat4 projection = glm::ortho(0.0f, static_cast<GLfloat>(200),
		static_cast<GLfloat>(270), 0.0f, -1.0f, 1.0f);
	ResourceManager::GetShader("sprite").SetMatrix4("projection", projection, GL_TRUE);
	ResourceManager::GetShader("text").SetMatrix4("projection", projection, GL_TRUE);
	ResourceManager::GetShader("text_outline").SetMatrix4("projection", projection, GL_TRUE);
	glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);
	glViewport(0, 0, 200, 270);
	glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
}

GLuint CardFramebuffer::StopRender()
{
	glm::mat4 projection = glm::ortho(0.0f, static_cast<GLfloat>(mainScreen.x),
		static_cast<GLfloat>(mainScreen.y), 0.0f, -1.0f, 1.0f);
	ResourceManager::GetShader("sprite").SetMatrix4("projection", projection, GL_TRUE);
	ResourceManager::GetShader("text").SetMatrix4("projection", projection, GL_TRUE);
	ResourceManager::GetShader("text_outline").SetMatrix4("projection", projection, GL_TRUE);

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glViewport(0, 0, mainScreen.x, mainScreen.y);
	return texColorBuffer;
}
