#pragma once
#include <glew.h>
#include <GLFW/glfw3.h>
#include "SpriteRenderer.h"
#include "TextRenderer.h"
#include "CardRenderer.h"
#include "ElementUI.h"
#include "TextPanel.h"
#include "Button.h"
#include "CollectionViewer.h"
#include "DeckViewer.h"
#include <vector>
#include "Player.h"

class UI : 
	public ElementUI
{
public:
	std::vector<TextPanel> TPelems;
	std::vector<Button> Belems;
	std::vector<CollectionViewer> CVelems;
	std::vector<DeckViewer> DVelems;

	CardRenderer cd_renderer;
	void AddTextPanel(std::string text, glm::vec2 pos, glm::vec2 size, Texture2D texture);
	void AddCollectionViewer( glm::vec2 pos, glm::vec2 size, Texture2D texture);
	void AddDeckViewer(glm::vec2 pos, glm::vec2 size, Texture2D texture);
	void AddButton(std::string text, glm::vec2 pos, glm::vec2 size, Texture2D texture, Action *com);
	void Draw(SpriteRenderer &s_renderer, TextRenderer &t_renderer, glm::vec2 screen);
	void CheckButtons(glm::vec2, glm::vec2 screen);
	void UpdateMouse(glm::vec2 mousecoord);
	void Pick(Card* card);
	void UnPick();
	UI(Player* player, Player* opp);
	~UI();
private:
	Card* card;
	glm::vec2 cardPosition;
	bool pressed;
	Player* player;
	Player* opponent;
};

