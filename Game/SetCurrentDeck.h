#pragma once
#include "Action.h"
#include "DeckList.h"
class SetCurrentDeck :
	public Action
{
public:
	void Execute();
	SetCurrentDeck(DeckList* dl, int i);
	~SetCurrentDeck();
private:
	DeckList* dl;
	int num;
};

