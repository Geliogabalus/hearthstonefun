#include "DeckList.h"



void DeckList::SetCurrentDeck(std::string name)
{
	for (int i = 0; i < decks.size(); i++) {
		if (decks[i]->GetName() == name) {
			currentDeck = i;
		}
	}
}

void DeckList::SetCurrentDeck(int num)
{
	currentDeck = num;
}

DeckList::DeckList()
{
	currentDeck = -1;
}


DeckList::~DeckList()
{
}
