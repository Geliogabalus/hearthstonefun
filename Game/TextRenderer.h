#pragma once
#include <map>

#include <glew.h>
#include <glm/glm.hpp>
#include "Renderer.h"
#include "texture.h"
#include "shader.h"

/// Holds all state information relevant to a character as loaded using FreeType
struct Character {
	GLuint TextureID;   // ID handle of the glyph texture
	glm::ivec2 Size;    // Size of glyph
	glm::ivec2 Bearing; // Offset from baseline to left/top of glyph
	GLuint Advance;     // Horizontal offset to advance to next glyph
};

class TextRenderer :
	public Renderer
{
public:
	std::map<GLchar, Character> Characters;
	TextRenderer(GLuint width, GLuint height);
	GLuint FontSize;
	void Load(std::string font, GLuint fontSize);
	// Renders a string of text using the precompiled list of characters
	void DrawText(std::string text, GLfloat x, GLfloat y, GLfloat scale, glm::vec3 color = glm::vec3(1.0f));
	void DrawOutlinedText(std::string text, GLfloat x, GLfloat y, GLfloat scale, glm::vec3 color = glm::vec3(1.0f));
	void Draw();
	~TextRenderer();
};