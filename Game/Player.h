#pragma once
#include <vector>
#include "Card.h"
#include "DeckList.h"
class Player
{
public:
	Player();
	~Player();	
	void AddDeck(std::string name);
	DeckList* GetDeckList() {
		return dlist;
	}
	std::vector<Card> Hand;
private:
	DeckList* dlist;
	int mana;
};

