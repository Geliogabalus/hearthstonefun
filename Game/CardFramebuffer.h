#pragma once
#include <glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <iostream>
#include "ResourceManager.h"
class CardFramebuffer
{
public:
	static GLuint framebuffer;
	static GLuint texColorBuffer;
	static glm::vec2 mainScreen;
	static void InitFramebuffer();
	static void StartRender();
	static GLuint StopRender();
private:
	CardFramebuffer() {};
	
};

