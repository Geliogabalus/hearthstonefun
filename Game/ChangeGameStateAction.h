#pragma once
#include "Action.h"
class ChangeGameStateAction
	: public Action
{
public:
	void Execute();
	ChangeGameStateAction(GameState state, Game *game);
	~ChangeGameStateAction();
private:
	Game* game;
	GameState state;
};

