#include "ChangeGameStateAction.h"
#include "Game.h"




void ChangeGameStateAction::Execute()
{
	game->State = state;
}

ChangeGameStateAction::ChangeGameStateAction(GameState state, Game* game)
{
	this->state = state;
	this->game = game;
}

ChangeGameStateAction::~ChangeGameStateAction()
{
}
