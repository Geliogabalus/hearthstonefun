#include "Card.h"

Card::Card(Texture2D & art, glm::vec2 artOffset, CARD_TYPE type, std::string name, std::string text, Stats baseStats, int ID)
{
	this->ID = ID;
	this->Name = name;
	this->type = type;
	this->art = art;
	this->artOffset = artOffset;
	this->Text = text;
	this->baseStats = baseStats;
}


Card::~Card()
{
}
