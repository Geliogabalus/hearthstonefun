#pragma once
#include "Action.h"
#include "UI.h"
class PickCard :
	public Action
{
public:
	void Execute();
	PickCard(UI* ui, Card *card);
	~PickCard();
private:
	UI* ui;
	Card* card;
};

