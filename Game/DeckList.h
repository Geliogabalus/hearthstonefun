#pragma once
#include "Deck.h"
class DeckList
{
public:
	void CreateDeck(std::string name) {
		decks.push_back(new Deck(name));
	}
	void DeleteDeck(int num) {
		decks.erase(decks.begin() + num);
	}
	void SetCurrentDeck(int num);
	void SetCurrentDeck(std::string name);
	Deck* GetCurrentDeck() {
		return decks[currentDeck];
	}
	void DropCurrentDeck() {
		currentDeck = -1;
	}
	int GetCurrentDeckInd() {
		return currentDeck;
	}
	bool CheckCurrentDeck() {
		if (currentDeck == -1) {
			return false;
		}
		else {
			return true;
		}
	}
	std::vector<Deck*> const GetDecks() {
		return decks;
	}
	int Size() {
		return decks.size();
	}
	DeckList();
	~DeckList();
private:
	int currentDeck;
	std::vector<Deck*> decks;
};

