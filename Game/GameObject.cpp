#include "GameObject.h"


GameObject::GameObject()
	: Position(0, 0), Size(1, 1), Velocity(0.0f), Color(1.0f), Rotation(0.0f), Sprite(), IsSolid(false), Destroyed(false) { }

GameObject::GameObject(glm::vec2 pos, glm::vec2 size, Texture2D sprite, glm::vec3 color, glm::vec2 velocity)
	: Position(pos), Size(size), Velocity(velocity), Color(color), Rotation(0.0f), Sprite(sprite), IsSolid(false), Destroyed(false) { }

void GameObject::Draw(SpriteRenderer &renderer)
{
	renderer.DrawSprite(this->Sprite, this->Position, this->Size, this->Rotation, this->Color);
}

void GameObject::BorderCollisionCheck(glm::vec2 disp)
{
	if (Position.x + Size.x > disp.x)
	{
		Position.x = disp.x - Size.x;
		Velocity.x = -Velocity.x;
	}
	if (Position.y + Size.y > disp.y)
	{
		Position.y = disp.y - Size.y;
		Velocity.y = -Velocity.y;
	}
	if (Position.x < 0)
	{
		Position.x = 0;
		Velocity.x = -Velocity.x;
	}
	if (Position.y < 0)
	{
		Position.y = 0;
		Velocity.y = -Velocity.y;
	}
}
