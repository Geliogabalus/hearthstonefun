#include "Player.h"



Player::Player()
{
	dlist = new DeckList();
	Card yeti = Card(ResourceManager::GetTexture("yeti"), glm::vec2(-30, -50), MINION, "Yeti", "Yeti is the best card", { 4, 5, 4 }, 1);
	AddDeck("Test");
	dlist->SetCurrentDeck("Test");
	Deck* deck = dlist->GetCurrentDeck();
	deck->addCard(yeti);
	deck->addCard(yeti);
	deck->addCard(yeti);
	dlist->DropCurrentDeck();
}


Player::~Player()
{
}

void Player::AddDeck(std::string name)
{
	dlist->CreateDeck(name);
}
