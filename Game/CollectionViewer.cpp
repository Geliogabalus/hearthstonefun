#include "CollectionViewer.h"
#include "CardView.h"
#include "PickCard.h"

void CollectionViewer::AddCard(Card* card)
{
	if (Collection[pagescount].size() < 8) {
		Collection[pagescount].push_back(card);
	}
	else {
		pagescount++;
		Collection.push_back(std::vector<Card*>());
		Collection[pagescount].push_back(card);
	}
}

void CollectionViewer::btnCheck(glm::vec2 mouse)
{
	for (int i = 0; i < cviews.size(); i++) {
		if (((mouse.x > cviews[i]->Position.x) && (mouse.x < cviews[i]->Position.x + cviews[i]->Size.x)) &&
			((mouse.y > cviews[i]->Position.y) && (mouse.y < cviews[i]->Position.y + cviews[i]->Size.y))) {
			cviews[i]->DoCommand();
		}
	}
}

void CollectionViewer::Draw(SpriteRenderer & s_renderer, TextRenderer &t_renderer, glm::vec2 screen)
{
	s_renderer.DrawSprite(Texture, glm::round(Position * screen), glm::round(Size * screen));
	GLfloat xb = Position.x * screen.x ;
	GLfloat yb = Position.y * screen.y + 20;
	GLfloat x, y;
	for (int i = 0; i < Collection[currentpage].size(); i++) {
		if (i < 4) {
			x = xb + 20 + i * 220;
			y = yb;
		}
		else {
			x = xb + 20 + (i - 4) * 220;
			y = yb + 290;
		}
		for (int i = 0; i < cviews.size(); i++) {
			cviews[i]->Position = glm::vec2(x, y);
			cviews[i]->Size = glm::vec2(200, 270);
			cviews[i]->Draw(s_renderer, t_renderer, screen);
		}
	}
}

CollectionViewer::CollectionViewer(glm::vec2 pos, glm::vec2 size, Texture2D texture, ElementUI* mainUI)
{
	Position = pos;
	Size = size;
	Texture = texture;
	this->mainUI = mainUI;
	page = 0;
	pagescount = 0;
	currentpage = 0;
	Collection.push_back(std::vector<Card*>());
	Card* yeti = new Card(ResourceManager::GetTexture("yeti"), glm::vec2(-30, -50), MINION, "Yeti", "Yeti is the best card", {4, 5, 4}, 1);
	AddCard(yeti);
	CreateCV();
}

CollectionViewer::~CollectionViewer()
{
}

void CollectionViewer::CreateCV()
{
	for (int i = 0; i < Collection[currentpage].size(); i++) {
		cviews.push_back(new CardView("", glm::vec2(0, 0), glm::vec2(0, 0), ResourceManager::GetTexture("btn"), new PickCard((UI*)mainUI, Collection[currentpage][i]),Collection[currentpage][i]));
	}
}
