#pragma once
#include "ElementUI.h"
class TextPanel :
	public ElementUI
{
public:
	std::string Text;
	TextPanel(std::string text, glm::vec2 pos, glm::vec2 size, Texture2D& texture);
	void Draw(SpriteRenderer &s_renderer,TextRenderer &t_renderer, glm::vec2 screen);
	~TextPanel();
};

