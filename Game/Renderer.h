#pragma once
#include <glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "Shader.h"
class Renderer
{
public:
	Renderer();
	~Renderer();
	virtual void Draw() = 0;
protected:
	Shader shader;
	GLuint pointsVAO;
	GLuint VBO;
};

