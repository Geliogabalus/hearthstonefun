#include "TextPanel.h"


TextPanel::TextPanel(std::string text, glm::vec2 pos, glm::vec2 size, Texture2D& texture)
{
	Text = text;
	Position = pos;
	Size = size;
	Texture = texture;
}

void TextPanel::Draw(SpriteRenderer & s_renderer,TextRenderer &t_renderer, glm::vec2 screen)
{
	s_renderer.DrawSprite(ResourceManager::GetTexture("btn"), Position * screen, Size * screen);
	glm::vec2 pos = Position * screen;
	glm::vec2 sz = Size * screen;
	GLfloat y = pos.y + ((sz.y - t_renderer.FontSize) / 2);
	GLfloat x = pos.x + (sz.x - t_renderer.FontSize * (Text.size() / 2)) / 2;
	t_renderer.DrawText(Text, x, y, 1, glm::vec3(0, 0, 0));
}

TextPanel::~TextPanel()
{
}
