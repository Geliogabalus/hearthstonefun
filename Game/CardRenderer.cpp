#include "CardRenderer.h"



CardRenderer::CardRenderer()
{
}

void CardRenderer::DrawCard(SpriteRenderer &s_renderer, TextRenderer &t_renderer, GLfloat x, GLfloat y, Card* card)
{
	CardFramebuffer::StartRender();
	s_renderer.DrawSprite(card->art, card->artOffset, glm::vec2(200, 270));
	switch (card->type) {
	case MINION:
		s_renderer.DrawSprite(ResourceManager::GetTexture("n_mask"), glm::vec2(0, 0), glm::vec2(200, 270));
		break;
	case SPELL:
		break;
	}	
	
	t_renderer.DrawText(card->Text, 40, 200, 0.5, glm::vec3(0, 0, 0));
	
	t_renderer.DrawText(std::to_string(card->baseStats.cost), 20, 10, 2, glm::vec3(1, 1, 1));

	t_renderer.DrawText(std::to_string(card->baseStats.attack), 20, 225, 2, glm::vec3(1, 1, 1));

	t_renderer.DrawText(std::to_string(card->baseStats.health), 165, 225, 2, glm::vec3(1, 1, 1));

	GLuint tex = CardFramebuffer::StopRender();
	ResourceManager::GetShader("sprite").Use().SetInteger("iscard", 1);
	s_renderer.DrawSpriteTexID(tex, glm::vec2(glm::round(x), glm::round(y)), glm::vec2(200, 270));
	ResourceManager::GetShader("sprite").Use().SetInteger("iscard", 0);
}


CardRenderer::~CardRenderer()
{
}
